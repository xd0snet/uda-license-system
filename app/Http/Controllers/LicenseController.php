<?php

namespace App\Http\Controllers;

use App\Country;
use App\License;
use App\State;
use Illuminate\Http\Request;
use Validator;

class LicenseController extends Controller
{
    //
    public function getCreateLicense(Request $request){

        $states = State::pluck('name', 'id');
        $country = Country::pluck('name', 'id');

        $data = [
            'title' => 'Create New License',
            'states' => $states,
            'country' => $country
        ];

        return view('license.create_license', $data);
    }

    public function postCreateLicense(Request $request){

        $validate = Validator::make($request->all(),
        [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'gender' => 'required',
            'birthday' => 'required',
            'status' => 'required',
            'license_type' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postcode' => 'required',
            'country' => 'required',
        ]);

        if($validate->fails()){
            return redirect()
                    ->back()
                    ->withErrors($validate);
        } else {

            $license = new License();
            $license->first_name = $request->first_name;
            $license->last_name = $request->last_name;
            $license->gender = $request->gender;
            $license->birthday = $request->birthday;
            $license->status = $request->status;
            $license->license_type = $request->license_type;
            $license->street_address = $request->address;
            $license->city = $request->city;
            $license->state = $request->state;
            $license->postcode = $request->postcode;
            $license->country = $request->country;
            $license->save();

            return redirect()
                    ->back()
                    ->with('success_message', 'New data created');

        }

    }

    public function getAll(Request $request) {
        $licenses = License::all();

        $data = [
            'title' => 'All Licenses',
            'licenses' => $licenses,
        ];

        return view('license.all', $data);
    }

    public function getDelete(Request $request, $id){
        $license = License::findOrFail($id);
        $license->delete();

        return redirect()
            ->route('license.all')
            ->with('success_message', 'Data has been deleted');
    }


    public function getModifyLicense(Request $request, $id){
        $license = License::findOrFail($id);

        $states = State::pluck('name', 'id');
        $country = Country::pluck('name', 'id');

        $data = [
            'title' => 'Modify License',
            'states' => $states,
            'country' => $country,
            'license' => $license
        ];

        return view('license.modify_license', $data);
    }

    public function postModifyLicense(Request $request, $id){
        $license = License::findOrFail($id);

        $validate = Validator::make($request->all(),
            [
                'first_name' => 'required|min:2',
                'last_name' => 'required|min:2',
                'gender' => 'required',
                'birthday' => 'required',
                'status' => 'required',
                'license_type' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
                'postcode' => 'required',
                'country' => 'required',
            ]);

        if($validate->fails()){
            return redirect()
                ->back()
                ->withErrors($validate);
        } else {

            $license->first_name = $request->first_name;
            $license->last_name = $request->last_name;
            $license->gender = $request->gender;
            $license->birthday = $request->birthday;
            $license->status = $request->status;
            $license->license_type = $request->license_type;
            $license->street_address = $request->address;
            $license->city = $request->city;
            $license->state = $request->state;
            $license->postcode = $request->postcode;
            $license->country = $request->country;
            $license->save();

            return redirect()
                ->back()
                ->with('success_message', 'Data has been modified');

        }

    }

    public function getView(Request $request, $id){
        $license = License::findOrFail($id);
        $data = [
            'title' => 'View License',
            'license' => $license,
        ];

        return view('license.view', $data);
    }

}
