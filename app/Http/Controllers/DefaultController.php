<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultController extends Controller
{
    //
    public function getIndex(Request $request){
        $data = [
          'title' => 'Dashboard',
        ];

        return view('default.index', $data);
    }
}
