<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    //
    protected $table = 'licenses';

    public function state_data(){
        return $this->belongsTo(State::class, 'state', 'id');
    }

    public function country_data(){
        return $this->belongsTo(Country::class, 'country', 'id');
    }
}
