@extends('layouts.default')

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">All Licenses</h4>
            <div class="table-responsive m-t-40">
                @if(\Session::has('success_message'))
                    <div class="alert alert-success">
                        {{ \Session::get('success_message')  }}
                    </div>
                @endif

                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>State</th>
                        <th>Country</th>
                        <th>License Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @forelse($licenses as $license)
                          <tr>
                              <td>{{ $license->first_name  }}</td>
                              <td>{{ $license->last_name  }}</td>
                              <td>{{ $license->state_data->name  }}</td>
                              <td>{{ $license->country_data->name  }}</td>
                              <td>
                                  @if($license->license_type == 0)
                                      Car
                                  @else
                                      Motorcycle
                                  @endif
                              </td>
                              <td>
                                  @if($license->status == 0)
                                      <span class="label label-danger">Pending</span>
                                  @else
                                      <span class="label label-success">Approved</span>
                                  @endif
                              </td>
                              <td>
                                  <a href="{{ route('license.view', [$license->id])  }}" class="btn btn-sm btn-primary">View</a>
                                  <a href="{{ route('license.modify', [$license->id])  }}" class="btn btn-sm btn-warning">Modify</a>
                                  <a href="{{ route('license.delete', [$license->id])  }}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')">Delete</a>
                              </td>
                          </tr>
                      @empty
                          <tr>
                             <td colspan="7">No data was found</td>
                          </tr>
                      @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.min.js')  }}"></script>
    <script>
        $(document).ready(function(){
            $('#myTable').DataTable();
        });
    </script>
@endsection