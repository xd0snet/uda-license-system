@extends('layouts.default')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Create License</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('license.create') }}" method="post">
                        @csrf
                        <div class="form-body">
                            <h3 class="card-title">Person Information</h3>
                            <hr>

                            @if(\Session::has('success_message'))
                                <div class="alert alert-success">
                                    {{ \Session::get('success_message')  }}
                                </div>
                            @endif

                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('first_name')) has-danger @endif ">
                                        <label class="control-label">First Name</label>
                                        <input type="text" id="firstName" class="form-control @if ($errors->has('first_name')) form-control-danger @endif" placeholder="John doe" name="first_name">
                                        @if ($errors->has('first_name'))
                                            <small class="form-control-feedback">{{ $errors->first('first_name') }}</small>
                                        @endif
                                         </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group  @if ($errors->has('last_name')) has-danger @endif ">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" id="lastName" class="form-control @if ($errors->has('last_name')) form-control-danger @endif " placeholder="12n" name="last_name">
                                        @if ($errors->has('last_name'))
                                            <small class="form-control-feedback">{{ $errors->first('last_name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('gender')) has-danger @endif ">
                                        <label class="control-label">Gender</label>
                                        <select class="form-control custom-select @if ($errors->has('gender')) form-control-danger @endif" name="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select></div>
                                        @if ($errors->has('gender'))
                                            <small class="form-control-feedback">{{ $errors->first('gender') }}</small>
                                        @endif
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('birthday')) has-danger @endif">
                                        <label class="control-label">Date of Birth</label>
                                        <input type="date" class="form-control  @if ($errors->has('birthday')) form-control-danger @endif" placeholder="dd/mm/yyyy" name="birthday">
                                        @if ($errors->has('birthday'))
                                            <small class="form-control-feedback">{{ $errors->first('birthday') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('status')) has-danger @endif">
                                        <label class="control-label">Status</label>
                                        <select class="form-control custom-select  @if ($errors->has('status')) form-control-danger @endif" data-placeholder="Status" tabindex="1" name="status">
                                            <option value="1">Approved</option>
                                            <option value="0">Pending</option>
                                        </select>
                                        @if ($errors->has('status'))
                                            <small class="form-control-feedback">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('license_type')) has-danger @endif">
                                        <label class="control-label">License Type</label>
                                        <div class="m-b-10">
                                            <label class="custom-control custom-radio">
                                                <input id="radio1" value="0" type="radio" name="license_type" class="custom-control-input">
                                                <span class="custom-control-label">Car</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input id="radio2" value="1" type="radio" name="license_type"  class="custom-control-input">
                                                <span class="custom-control-label">Motorcycle</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('license_type'))
                                            <small class="form-control-feedback">{{ $errors->first('license_type') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <h3 class="box-title m-t-40">Address</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group @if ($errors->has('address')) has-danger @endif">
                                        <label>Address</label>
                                        <input type="text" name="address"  class="form-control @if ($errors->has('address')) form-control-danger @endif">
                                        @if ($errors->has('address'))
                                            <small class="form-control-feedback">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('city')) has-danger @endif">
                                        <label>City</label>
                                        <input type="text" name="city" class="form-control @if ($errors->has('city')) form-control-danger @endif">
                                        @if ($errors->has('city'))
                                            <small class="form-control-feedback">{{ $errors->first('city') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('state')) has-danger @endif">
                                        <label>State</label>
                                        <select class="state-select2 form-control @if ($errors->has('state')) form-control-danger @endif" name="state">
                                           @foreach($states as $id => $name)
                                               <option value="{{ $id  }}">{{ $name }}</option>
                                           @endforeach
                                        </select>
                                        @if ($errors->has('state'))
                                            <small class="form-control-feedback">{{ $errors->first('state') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('postcode')) has-danger @endif">
                                        <label>Post Code</label>
                                        <input type="text" class="form-control @if ($errors->has('postcode')) form-control-danger @endif" name="postcode">
                                        @if ($errors->has('postcode'))
                                            <small class="form-control-feedback">{{ $errors->first('postcode') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group @if ($errors->has('country')) has-danger @endif">
                                        <label>Country</label>
                                        <select class="country-select2 form-control  @if ($errors->has('country')) form-control-danger @endif" name="country">
                                            @foreach($country as $id => $name)
                                                <option value="{{ $id  }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                            <small class="form-control-feedback">{{ $errors->first('country') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-inverse">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

@endsection

@section('script')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script>
        $(document).ready(function(){
            $('.state-select2').select2();
            $('.country-select2').select2();
        });
    </script>

@endsection