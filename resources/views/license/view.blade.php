@extends('layouts.default')

@section('content')
    <div class="row">
        <!-- .col -->
        <div class="col-md-8 col-lg-8 col-xlg-4">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <h3 class="box-title m-b-0">{{ $license->first_name  }} {{ $license->last_name  }}</h3> <small> @if($license->license_type == 0)Car @else Motorcycle @endif</small>
                        <p>&nbsp;</p>
                        <strong>Birthday</strong>
                        <p>{{ $license->birthday  }}</p>
                        <strong>Gender</strong>
                        <p>{{ ucfirst($license->gender)  }}</p>
                        <strong>Address</strong>
                        <address>
                            {{ $license->street_address  }}<br />
                            {{ $license->city  }}, {{ $license->state_data->name  }},
                            {{ $license->postcode  }}<br />>
                            {{ $license->country_data->name  }}
                            <br/>
                            <abbr title="Phone">P:</abbr> (123) 456-7890
                        </address>
                        <small class="label @if($license->status == 0) label-danger @else label-success @endif">@if($license->status == 0) Pending @else Approved @endif</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection