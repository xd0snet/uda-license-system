<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//authorized pages / need to login first before access this page
Route::group(['middleware' => ['auth']], function(){

    Route::get('/dashboard', 'DefaultController@getIndex')->name('dashboard');
    Route::get('/license/create', 'LicenseController@getCreateLicense')->name('license.create');
    Route::post('/license/create', 'LicenseController@postCreateLicense')->name('license.create');
    Route::get('/license/all', 'LicenseController@getAll')->name('license.all');
    Route::get('/license/delete/{id}', 'LicenseController@getDelete')->name('license.delete');
    Route::get('/license/modify/{id}', 'LicenseController@getModifyLicense')->name('license.modify');
    Route::post('/license/modify/{id}', 'LicenseController@postModifyLicense')->name('license.modify');
    Route::get('/license/view/{id}', 'LicenseController@getView')->name('license.view');

});